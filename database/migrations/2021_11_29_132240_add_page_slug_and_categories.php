<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPageSlugAndCategories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pages',function (Blueprint $table){
            $table->string('slug')->unique()->nullable();
            $table->string('image')->nullable();
        });

        Schema::create('categories',function (Blueprint $table){
            $table->id();
            $table->string('name');
        });

        Schema::create('page_categories',function (Blueprint $table){
            $table->unsignedBigInteger('page_id');
            $table->unsignedBigInteger('category_id');

            $table->foreign('page_id')->references('id')->on('pages');
            $table->foreign('category_id')->references('id')->on('categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('page_categories');
        Schema::dropIfExists('categories');
        Schema::dropColumns('pages',['slug','image']);
    }
}
