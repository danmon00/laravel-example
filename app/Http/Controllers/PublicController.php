<?php

namespace App\Http\Controllers;

use App\Mail\TestMail;
use App\Models\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class PublicController extends Controller
{

    public function pages(){
        return Page::query()->get();
    }

    public function pagesWeb(){
        $pages =  Page::all();
        return view('pages', ['pages'=>$pages]);
    }

    public function login(Request $request){
        if($request->has('user')){
            $rules = [
                'user'=>'required|email',
                'pass'=>'required|min:4'
            ];

            $messages = ['user.email'=>'correo invalido'];
            $validator = Validator::make($request->only('user','pass'),$rules,$messages);
            if($validator->fails()){
                dd($validator->errors());
                return redirect(url('/login-frm'))->with($validator->errors());
                return response()->json(['status'=>401,'message'=>$validator->errors()]);
            }else{
                $credentials=['email'=>$request->get('user'),'password'=>$request->get('pass')];
               if(Auth::attempt($credentials)){
                   return redirect(url('/pages'));
               }else
                   echo 'error logueando';
            }
        }else{
            return view('login');
        }

    }

    public function logout(Request $request){
        Auth::logoutCurrentDevice();
        return redirect(url('/login-frm'));
    }


    public function sendMail(Request $request){
        if(!Auth::guest()) {
            Mail::to(Auth::user()->email)
                ->send(new TestMail(
                    Auth::user()->name,
                    'cualquer cosa',
                    route('pages',['mail'=>Auth::user()->email])
                ));

            if (Mail::failures()) {
                echo 'fallo';
            } else
                echo 'enviado';
        }else
            abort('401');
    }
}
