<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    use HasFactory;

    const ORIGIN_AUTHOR = 'Author';
    const ORIGIN_BOOK = 'Book';
    const ORIGIN_WEB = 'Web';

    public function author(){
        return $this->belongsTo(User::class);
    }

    public function contentType(){
        return $this->belongsTo(ContentType::class);
    }

    public function scopeBook($query){
       return $query->where(['origin'=>\App\Models\Page::ORIGIN_BOOK]);
    }
    public function scopeAuthorFilter($query){
        return $query->where(['origin'=>\App\Models\Page::ORIGIN_AUTHOR]);
    }

    public static function boot()
    {
        parent::boot();
        self::created(function ($model){
            $model->contentType->total +=1;
            $model->contentType->save();
        });
        self::updated(function ($model){
            $model->price = rand(0,1000000);
        });

    }

}
