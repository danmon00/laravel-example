<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Email Test</title>
</head>
<body>

    <h1>{{$title}}</h1>
    <p>{{$body}}</p>
    <p>
        Link: <a href="{{$url}}">Enlace</a>
    </p>
</body>
</html>
