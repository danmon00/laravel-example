@empty($rows)
    Sin datos
@else
<div class="table">
    <table>
        <thead>
        <tr>
            @foreach($headers  as $key=>$val)
                <th>
                    {{$val}}
                </th>
            @endforeach
        </tr>
        </thead>
        <tbody>

            @foreach($headers  as $key=>$val)
                <tr>
                    @foreach($rows  as $row)
                        <td>{{$row[$key]}}</td>
                    @endforeach
                </tr>
            @endforeach

        </tbody>
    </table>
</div>
@endempty
